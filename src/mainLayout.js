import { Component, useState } from 'react';
import { 
  BrowserRouter as 
  Router, 
  Route, 
  Link } from "react-router-dom";
import { 
  Layout, 
  Menu,
  Table,
  List,
  Card,
  Avatar, 
  Breadcrumb,  
} from 'antd';
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
  SketchOutlined,
  HeatMapOutlined,
  AppstoreOutlined
} from '@ant-design/icons';

import Customers from './customers/customers';
import Collections from './collections/collections';
import Orders from './orders/orders';
import Products from './products/products';
import AddProduct from './products/add';
import AddCollection from './collections/add';
import AddCustomer from './customers/add';
import ShowCustomer from './customers/show';
import ShowProduct from './products/show';
import ShowOrder from './orders/show';


const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const MainLayout = () => {
  const [collapsed, onCollapse] = useState(false);
  const [context, onContextClicked] = useState('customers');
    
  return (
    <Router>
      <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
            <Menu.Item key="1" icon={<PieChartOutlined />}>
              Main App
            </Menu.Item>
            <SubMenu 
              key="sub1" 
              icon={<UserOutlined />} 
              title="Customers"
            >
              <Menu.Item key="11" onClick={() => onContextClicked('customers')}>
                <Link to="/customers">Home</Link>
              </Menu.Item>
              <Menu.Item key="12">
                <Link to="/customers/add">Add Customer</Link>
              </Menu.Item>
            </SubMenu>
            <SubMenu 
              key="sub2" 
              icon={<SketchOutlined /> } 
              title="Products"
            >
              <Menu.Item key="21" onClick={() => onContextClicked('products')}>
                <Link to="/products">Home</Link>
              </Menu.Item>
              <Menu.Item key="22">
                <Link to="/products/add">Add Product</Link>
              </Menu.Item>
            </SubMenu>
            <SubMenu 
              key="sub3" 
              icon={<HeatMapOutlined />} 
              title="Orders"
            >
              <Menu.Item key="31" onClick={() => onContextClicked('orders')}>
                <Link to="/orders">Home</Link>
              </Menu.Item>
              {/* <Menu.Item key="32" onClick={() => onContextClicked('orders')}>
                <Link to="/orders/make">Make Order</Link>
              </Menu.Item> */}
            </SubMenu>
            <SubMenu 
              key="sub4" 
              icon={ <AppstoreOutlined /> } 
              title="Collections"
            >
              <Menu.Item key="41" onClick={() => onContextClicked('collections')}>
                <Link to="/collections">Home</Link>
              </Menu.Item>
              {/* <Menu.Item key="42" onClick={() => onContextClicked('collections')}>
                <Link to="/collections/add">Add Collection</Link>
              </Menu.Item> */}
            </SubMenu>
            <Menu.Item key="9" icon={<FileOutlined />}>
              Files
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} />
          <Content style={{ margin: '0 16px' }}>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
              <Route exact path="/customers" component={Customers} />
              <Route exact path="/orders" component={Orders} />
              <Route exact path="/orders/show/:orderId" component={ShowOrder} />
              <Route exact path="/products" component={Products} />
              <Route exact path="/products/add" component={AddProduct} />
              <Route exact path="/collections" component={Collections} />
              <Route exact path="/customers/add" component={AddCustomer} />
              <Route exact path="/customers/show/:customerId" component={ShowCustomer} />
              <Route exact path="/products/show/:productId" component={ShowProduct} />
              <Route exact path="/collections/add" component={AddCollection} />
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Harrington App V1 ©2021 Harrington</Footer>
        </Layout>
      </Layout>
    </Router>
  );
}

export default MainLayout;