import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// const baseURL = 'http://localhost:3000';
const baseURL = 'https://11fo3t57q1.execute-api.us-east-1.amazonaws.com';

export const initialState = {
    loading: false,
    hasErrors: false,
    collections: []
}

const collectionsSlice = createSlice({
    name: 'collections',
    initialState,
    reducers: {
        getCollections: state => {
            state.loading = true
        },
        getCollectionsSuccess: (state, {payload}) => {
            state.collections = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        getCollectionsFailure: state => {
            state.loading = false;
            state.hasErrors = true;
        }
    }
});

export const { getCollections, getCollectionsSuccess, getCollectionsFailure } = collectionsSlice.actions;

export const collectionsSelector = state => state.collections;

export const fetchCollections = () => {
    return async dispatch => {
        dispatch(getCollections())

        try {
            const response = await fetch(`${baseURL}/dev/collections/all`)
            console.log("collections response: ", response)
            const collectionsResponse = await response.json();

            dispatch(getCollectionsSuccess(collectionsResponse.data))
        } catch (err) {
            dispatch(getCollectionsFailure())
        }
    }
}


export default collectionsSlice.reducer;
