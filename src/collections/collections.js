import { Component, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { collectionsSelector, fetchCollections } from './collectionsSlice';

import { 
    Row,
    Col,
    Table,
    Button
  } from 'antd';


import { UserAddOutlined } from '@ant-design/icons';

const Collections = () => {

    const dispatch = useDispatch()

    const { collections, loading, hasErrors } = useSelector(collectionsSelector);

    const columns = [
        {
          title: 'Title',
          width: 100,
          dataIndex: 'title',
          key: 'title',
          fixed: 'left',
        },
        {
          title: 'Description',
          width: 100,
          dataIndex: 'description',
          key: 'description',
          fixed: 'left',
        },
        {
          title: 'tags',
          dataIndex: 'tags',
          key: 'tags',
          width: 150,
        },
        {
          title: 'Action',
          key: 'operation',
          fixed: 'right',
          width: 100,
          render: () => <a>action</a>,
        },
      ];

    const data = [];

    collections.map((item, i) => {
        data.push({
            key: i,
            title: item.title,
            description: item.description,
            tags: item.tags        
        })
    })

    useEffect(() => {
        dispatch(fetchCollections())
      }, [dispatch])

    let history = useHistory();
    const handleClick = () => {
        history.push("/collections/add");
    }
  
    return (
        <div>
            <h3>Collections</h3>
            <Row>
                <Col span={8}></Col>
                <Col span={8}></Col>
                <Col span={8}>
                    <Button 
                        type="primary" 
                        icon={<UserAddOutlined />} 
                        size={'large'}
                        onClick={handleClick}
                    >
                Add Collection
                    </Button>
                </Col>
            </Row>
            <Table columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }} />
        </div>
    )
}

export default Collections;