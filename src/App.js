import { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';

import MainLayout from './mainLayout';

import logo from './logo.svg';
import ASimpleHook from './learn/hooks.1';

import { recipesSelector, fetchRecipes } from './slices/recipes';

import './App.css';

const App = () => {

  //update redux data
  const dispatch = useDispatch()

  // access redux data
  const { recipes, loading, hasErrors } = useSelector(recipesSelector)

  useEffect(() => {
    dispatch(fetchRecipes())
  }, [dispatch])

  const renderRecipes = () => {
    if (loading) return <p>Loading recipes...</p>
    if (hasErrors) return <p>Cannot display recipes</p>

    return recipes.map(recipe => 
        <div key={recipe.idMeal} className='tile'>
          <h2>{recipe.strMeal}</h2>
          <img src={recipe.strMealThumb} alt=''/>
        </div>
      )
  }

  return (
    <div className="App">
      <MainLayout />
    </div>
  );
}

export default App;
