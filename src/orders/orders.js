import { Component, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { 
    Table,
  } from 'antd';

import { ordersSelector, fetchOrders } from './ordersSlice';

const Orders = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const { orders, loading, hasErrors } = useSelector(ordersSelector);

    const onShowClicked = async (order) => {
      history.push(`/orders/show/${order.order_id}`)
    }

    const columns = [
        {
          title: 'Name',
          width: 100,
          dataIndex: 'name',
          key: 'name',
          fixed: 'left',
        },
        {
          title: 'Email',
          width: 100,
          dataIndex: 'email',
          key: 'email',
          fixed: 'left',
        },
        {
          title: 'Financial Status',
          dataIndex: 'financial_status',
          key: 'financial_status',
          width: 150,
        },
        {
          title: 'Paid at',
          dataIndex: 'paid_at',
          key: '2',
          width: 150,
        },
        {
          title: 'Action',
          key: 'operation',
          fixed: 'right',
          width: 100,
          render: (text, record) => <a onClick= { () => { 
            onShowClicked(record)
          }}>Show</a>,
        },
      ];

    const data = [];

    orders.map((item, i) => {
        data.push({
            key: i,
            order_id: item._id,
            name: item.name,
            email: item.email,
            financial_status: item.financial_status,
            paid_at: item.paid_at
        })
    })

    useEffect(() => {
        dispatch(fetchOrders())
      }, [dispatch])
    

    return (
        <div>
            <h3>Orders</h3>
            <Table columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }} />
        </div>
    )
}

export default Orders;