import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Descriptions, Badge }  from 'antd';
import {
    useParams
} from "react-router-dom";

import { findOneOrder, ordersSelector } from './ordersSlice';

const ShowOrder = () => {
    let { orderId } = useParams();
    let dispatch = useDispatch();

    const { selectedOrder } = useSelector(ordersSelector);

    useEffect(async () => {
        try {
            let resultProduct = await dispatch(findOneOrder(orderId));
        } catch (err) {
            console.log("findOneOrder Error: ", err)
        }
    }, [dispatch])

    console.log("selectedOrder: ", selectedOrder);
    const {name, email, financial_status, paid_at, amount } = selectedOrder && selectedOrder.data;
    let status = '';
    if (financial_status == 'pending') {
        status = 'processing';
    } else if (financial_status == 'void') {
        status = 'error';
    } else if (financial_status == 'paid') {
        status = 'success';
    }
    else {
        status = 'error';
    }
    return <div style={{background: 'white'}}>
        { selectedOrder && selectedOrder.loading &&
            <div>loading</div>
        }
        {   selectedOrder && !selectedOrder.loading &&
            <Descriptions 
            title="Order Info"
            style={{ marginTop: 20}}
            bordered
            >
                <Descriptions.Item label="Name" span={3}>{name}</Descriptions.Item>
                <Descriptions.Item label="Email" span={3}>{email}</Descriptions.Item>
                <Descriptions.Item label="Paid At" span={3}>{paid_at}</Descriptions.Item>
                <Descriptions.Item label="Amount" span={3}>{amount}</Descriptions.Item>
                <Descriptions.Item label="Financial Status" span={3}>
                    <Badge status={status} text={financial_status} />
                </Descriptions.Item>
            </Descriptions>
        }
    </div>
}

export default ShowOrder;