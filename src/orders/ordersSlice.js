import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// const baseURL = 'http://localhost:3000';
const baseURL = 'https://11fo3t57q1.execute-api.us-east-1.amazonaws.com';

export const initialState = {
    loading: false,
    hasErrors: false,
    orders: [],
    selectedOrder: {
        loading: false,
        hasErrors: false,
        data: {}
    },
    ordersForCustomer: {
        loading: false,
        hasErrors: false,
        data: []
    }
}

export const fetchOrders = () => {
    return async dispatch => {
        dispatch(getOrders())

        try {
            const response = await fetch(`${baseURL}/dev/orders/all`)
            console.log("orders response: ", response)
            const ordersResponse = await response.json();

            dispatch(getOrdersSuccess(ordersResponse.data))
        } catch (err) {
            dispatch(getOrdersFailure())
        }
    }
}

export const fetchOrdersForCustomer = createAsyncThunk('products/fetchOrdersForCustomer', async (customer_id) => {
    console.log("call fetchOrdersForCustomer")
    const response = await fetch(`${baseURL}/dev/orders/fetchOrdersForCustomer?customerId=${customer_id}`, {
        method: 'get'
    });

    console.log(">>>>>>> fetchOrdersForCustomer response: ", response);

    const fetchOrdersForCustomerResponse = await response.json();
    return fetchOrdersForCustomerResponse.data;
});

export const findOneOrder = createAsyncThunk('order/findOne', async (orderId) => {
    const response = await fetch(`${baseURL}/dev/orders/findOneOrder?orderId=${orderId}`, {
        method: 'get'
    });

    console.log(">>>>>>> findOneOrder response: ", response);

    const findOneOrderResponse = await response.json();
    return findOneOrderResponse.data;
});

const ordersSlice = createSlice({
    name: 'orders',
    initialState,
    reducers: {
        getOrders: state => {
            state.loading = true
        },
        getOrdersSuccess: (state, {payload}) => {
            state.orders = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        getOrdersFailure: state => {
            state.loading = false;
            state.hasErrors = true;
        }
    },

    extraReducers: {
        [fetchOrdersForCustomer.pending]: (state) => {
            console.log('>>>>>>. fetchOrdersForCustomer request pending');
            state.ordersForCustomer.loading = true;
        },
        [fetchOrdersForCustomer.fulfilled]: (state, action) => {
            console.log('>>>>>>. fetchOrdersForCustomer request fulfilled');
            console.log("fetchOrdersForCustomer request fulfilled action.payload: ", action.payload);
            state.ordersForCustomer.data = action.payload;
            state.ordersForCustomer.hasErrors = false;
            state.ordersForCustomer.loading = false;
        },
        [fetchOrdersForCustomer.reject]: (state) => {
            console.log('>>>>>>. fetchOrdersForCustomer request reject');
            state.ordersForCustomer.hasErrors = true;
        },
        [findOneOrder.pending]: (state) => {
            console.log('>>>>>>. findOneProduct request pending');
            state.selectedOrder.loading = true;
        },
        [findOneOrder.fulfilled]: (state, action) => {
            console.log('>>>>>>. findOneProduct request fulfilled');
            console.log("findOneProduct request fulfilled action.payload: ", action.payload);
            state.selectedOrder.data = action.payload[0];
            state.selectedOrder.hasErrors = false;
            state.selectedOrder.loading = false;
        },
        [findOneOrder.reject]: (state) => {
            console.log('>>>>>>. findOneProduct request reject');
            state.selectedOrder.hasErrors = true;
        },
    }
});

export const { getOrders, getOrdersSuccess, getOrdersFailure } = ordersSlice.actions;

export const ordersSelector = state => state.orders;


export default ordersSlice.reducer;
