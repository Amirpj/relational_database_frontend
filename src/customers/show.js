import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { 
    Descriptions, 
    Badge, 
    Select,
    Button 
}  from 'antd';
import {
    useParams
} from "react-router-dom";
import { useHistory } from 'react-router-dom';

import { findOneCustomer, customersSelector } from './customersSlice';
import { ordersSelector, fetchOrdersForCustomer } from '../orders/ordersSlice';


const ShowCustomer = () => {
    let history = useHistory();
    let dispatch = useDispatch();

    let [selectedOrderToView, setSelectedOrderToView ] = useState(null)
    let { customerId } = useParams();
    

    const { selectedCustomer } = useSelector(customersSelector);
    const { orders, ordersForCustomer } = useSelector(ordersSelector);

    const orderOptions = [];

    useEffect(async () => {
        try {
            let resultCustomer = await dispatch(findOneCustomer(customerId));
            let resultOrdersForCustomer = await dispatch(fetchOrdersForCustomer(customerId));
        } catch (err) {
            console.log("findOneCustomer/fetchOrdersForCustomer Error: ", err)
        }
    }, [dispatch])

    const handleSelectOrder = async (value) => {
        console.log("value: ", value, customerId);
        setSelectedOrderToView(value)
    }

    const handleViewCustomerOrder = async () => {
        if (!selectedOrderToView) {
            console.log('Please select order to view')
        } else {
            history.push(`/orders/show/${selectedOrderToView}`)
        }
    }

    console.log("ordersForCustomer: ", ordersForCustomer);
    console.log("selectedCustomer: ", selectedCustomer);

    ordersForCustomer && ordersForCustomer.data.map((item, i) => {
        orderOptions.push({
          key: i,
          value: item._id,
          name: item.name,
        })
      })

    const {f_name, l_name, email, company, phone, address, city, country, total_spent, total_orders} = selectedCustomer && selectedCustomer.data;
    return <div style={{background: 'white'}}>
        { selectedCustomer && selectedCustomer.loading &&
            <div>loading</div>
        }
        {   selectedCustomer && !selectedCustomer.loading &&
            <Descriptions 
            title="Customer Info"
            style={{ marginTop: 20}}
            bordered
            >
                <Descriptions.Item label="First Name" span={3}>{f_name}</Descriptions.Item>
                <Descriptions.Item label="Last Name" span={3}>{l_name}</Descriptions.Item>
                <Descriptions.Item label="Email" span={3}>{email}</Descriptions.Item>
                <Descriptions.Item label="Address" span={3}>{address}</Descriptions.Item>
                <Descriptions.Item label="Phone" span={3}>{phone}</Descriptions.Item>
                <Descriptions.Item label="Country" span={2}>{country}</Descriptions.Item>
                <Descriptions.Item label="City" span={2}>{city}</Descriptions.Item>
                <Descriptions.Item label="Status" span={3}>
                    <Badge status="success" text="Active" />
                </Descriptions.Item>
                <Descriptions.Item label="Total Order" span={3}>{total_orders}</Descriptions.Item>
                <Descriptions.Item label="Total amount spent" span={3}>Rs {total_spent}</Descriptions.Item>
                <Descriptions.Item label="Orders">
                    <Select
                        style={{ width: '100%' }}
                        placeholder="Please select"
                        onChange={handleSelectOrder}
                        options={orderOptions} 
                    />
                    <Button type='primary' onClick={handleViewCustomerOrder}>View</Button>
                </Descriptions.Item>
            </Descriptions>
        }
    </div>
}

export default ShowCustomer;