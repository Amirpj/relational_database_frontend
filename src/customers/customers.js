import { Component, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { unwrapResult } from '@reduxjs/toolkit';

import { useHistory } from 'react-router-dom';

import { 
    Row,
    Col,
    Table,
    Button,
    Space,
    Modal,
    Form, 
    Input,
    Radio,
    Select, 
    Divider
  } from 'antd';

import { UserAddOutlined } from '@ant-design/icons';

import { customersSelector, fetchCustomers, fetchCustomersNew, deleteCustomer, placeOrder } from './customersSlice';
import { productsSelector, fetchProducts } from '../products/productsSlice';

const OrderCreateForm = ({ visible, onCreate, onCancel, customer }) => {

  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchProducts())
  }, [dispatch])

  const options = [];
  const { products, loading, hasErrors } = useSelector(productsSelector);

  products.map((item, i) => {
    options.push({
        key: i,
        value: item._id,
        title: item.title,
        handle: item.handle,
        description: item.description,
        sku: item.sku
      })
  })

  const [form] = Form.useForm();

  const handleSelectProduct = (value) => {
    console.log(`selected ${value}`);
  }

  return (
    <Modal
      visible={visible}
      title="Place a new order"
      okText="Place an order"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={async () => {
        const { key, ...new_customer } = customer
        try {
          let values = await form.validateFields();
          let reset = await form.resetFields();
          onCreate({...new_customer, ...values});
          values = { ...new_customer, ...values };


          const placeOrderReponse = await dispatch(placeOrder(values));
          if(placeOrderReponse) {
            console.log(">>>>>>>>>>>>> placeOrderResponse", placeOrderReponse);
          }
        } catch(err) {
          console.log('Validate Failed:', err);
        }
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="name"
          label="Name"
          rules={[
            {
              required: true,
              message: 'Please input the name of order!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item name="financial_status" label="Financial Status">
          <Radio.Group>
            <Radio value="pending">pending</Radio>
            <Radio value="void">void</Radio>
            <Radio value="paid">paid</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item name="paid_at" label="Paid At" className="collection-create-form_last-form-item">
          <Input />
        </Form.Item>
        <Form.Item name="order_amount_total" label="Order Amount" className="collection-create-form_last-form-item">
          <Input />
        </Form.Item>
        <Form.Item name="order_product" label="Order Product" className="collection-create-form_last-form-item">

          <Select
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={handleSelectProduct}
            options={options}
        />
       </Form.Item>
        <Divider />
        <Form.Item>
          <div>Order going to be placed for <strong>{customer.name}</strong></div>
        </Form.Item>
      </Form>
    </Modal>
  );
};

const Customers = () => {
    //update redux data
    const dispatch = useDispatch();
    const [orderModelVisible, setOrderModelVisible] = useState(false);
    const [currentCustomer, setCurrentCustomer] = useState({});

    // access redux data
    const { customers, loading, hasErrors } = useSelector(customersSelector);

    const onDeleteClicked = async (customer) => {
        try {
            const deleteCustomerResult = await dispatch(deleteCustomer(customer));
            unwrapResult(deleteCustomerResult);
        } catch (err) {
            console.error('Failed to delete the customer: ', err)
        } finally {
          }
    }

    const onCreateOrderModel = (values) => {
        console.log('Received values of form: ', values);
        setOrderModelVisible(false);
    }

    const onShowClicked = async (customer) => {
      console.log(">>>customer: ", customer)
      history.push(`/customers/show/${customer.customer_id}`)
    }

    const columns = [
        {
            title: 'Name',
            width: 100,
            dataIndex: 'name',
            key: 'name',
            fixed: 'left',
        },
        {
            title: 'Email',
            width: 100,
            dataIndex: 'email',
            key: 'email',
            fixed: 'left',
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
            width: 150,
        },
        {
            title: 'City',
            dataIndex: 'city',
            key: 'city',
            width: 150,
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            width: 100,
        render: (text, record) =>  { 
            return (
                <Space size="middle">
                    <a onClick={() => onDeleteClicked(record)}>delete</a>
                    <a onClick={() => {
                       setOrderModelVisible(true)
                       setCurrentCustomer(record) 
                    }}>Place an order</a>
                    <a onClick={() => {onShowClicked(record)}}>Show</a>
                    <OrderCreateForm
                        visible={orderModelVisible}
                        customer={currentCustomer}
                        onCreate={onCreateOrderModel}
                        onCancel={() => {
                            setOrderModelVisible(false);
                        }}
                    />
                </Space>) 
        }
    }
    ];
    
    const data = [];
    customers.map((item, i) => {
        data.push({
            key: i,
            customer_id: item._id,
            name: item.name,
            email: item.email,
            address: item.address,
            city: item.city
        })
    })

    useEffect(() => {
        dispatch(fetchCustomersNew())
      }, [dispatch])


    let history = useHistory();
    const handleClick = () => {
        history.push("/customers/add");
    }

    return (
        <div>
            <h3>Customers</h3>
            <Row>
                <Col span={8}></Col>
                <Col span={8}></Col>
                <Col span={8}>
                    <Button 
                        type="primary" 
                        icon={<UserAddOutlined />} 
                        size={'large'}
                        onClick={handleClick}
                    >
                Add Customer
                    </Button>
                </Col>
            </Row>
            { data && <Table columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }} /> }
        </div>
    )
}

export default Customers;