import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { 
    Form, 
    Input, 
    InputNumber, 
    Button,
    Row,
    Col
    } from 'antd';

import { unwrapResult } from '@reduxjs/toolkit';

import { addNewCustomer } from './customersSlice';

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  
  /* eslint-disable no-template-curly-in-string */
  const validateMessages = {
    required: '${label} is required!',
    types: {
      email: '${label} is not a valid email!',
      number: '${label} is not a valid number!',
    },
    number: {
      range: '${label} must be between ${min} and ${max}',
    },
  };

const AddCustomer = () => {

    const dispatch = useDispatch();

    const [addCustomerRequestStatus, setAddCustomerRequestStatus] = useState('idle')


    const onFinish = async (values) => {
        const { customer } = values
        const canSave = [customer.first_name,customer.last_name,  customer.email, customer.address].every(Boolean) && addCustomerRequestStatus === 'idle';

        if(canSave) {
            try {
                const resultAction = await dispatch(addNewCustomer(customer));
                unwrapResult(resultAction);
            } catch (err) {
                console.error('Failed to save the post: ', err)
            } finally {
                setAddCustomerRequestStatus('idle');
              }
        } else {
            console.log("Please fill all the fields")
        }
      };

    return (
        <Row>
            <Col span={16}>
                <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                    <Form.Item name={['customer', 'first_name']} label="First Name" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name={['customer', 'last_name']} label="Last Name" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name={['customer', 'email']} label="Email" rules={[{ type: 'email' }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name={['customer', 'address']} label="Address">
                        <Input />
                    </Form.Item>
                    <Form.Item name={['customer', 'city']} label="City">
                        <Input />
                    </Form.Item>
                    <Form.Item name={['customer', 'province']} label="Province">
                        <Input />
                    </Form.Item>
                    <Form.Item name={['customer', 'country']} label="Country">
                        <Input />
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType="submit">
                        Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </Row> 
        
    )
}

export default AddCustomer