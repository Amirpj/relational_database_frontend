import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// const baseURL = 'http://localhost:3000';
const baseURL = 'https://11fo3t57q1.execute-api.us-east-1.amazonaws.com';

export const initialState = {
    loading: false,
    hasErrors: false,
    customers: [],
    selectedCustomer: {
        loading: false,
        hasErrors: false,
        data: {}
    }
}

export const fetchCustomersNew = createAsyncThunk('customers/fetchCutomers', async () => {
    const response = await fetch(`${baseURL}/dev/customers/all`) 
    const customersResponse = await response.json();
    return customersResponse.data;
})
export const fetchCustomers = () => {
    return async dispatch => {
        dispatch(getCustomers())

        try {
            const response = await fetch(`${baseURL}/dev/customers/all`)
            console.log("response: ", response)
            const customerResponse = await response.json();

            dispatch(getCustomersSuccess(customerResponse.data))
        } catch (err) {
            dispatch(getCustomersFailure())
        }
    }
}

export const addNewCustomer = createAsyncThunk('customers/addNewCustomer', async (newCustomer) => {
    const response = await fetch(`${baseURL}/dev/customers/addNewCustomer`, {
        method: 'post',
        body: JSON.stringify(newCustomer)
    })

    const addNewCustomerResponse = await response.json();
    return addNewCustomerResponse.data;
})


export const deleteCustomer = createAsyncThunk('customers/deleteCustomer', async (customerToDelete) => {
    const response = await fetch(`${baseURL}/dev/customers/deleteCustomer`, {
        method: 'delete',
        body: JSON.stringify(customerToDelete)
    })

    const deleteCustomerResponse = await response.json();
    return deleteCustomerResponse.data;
})


export const placeOrder = createAsyncThunk('customers/placeOrder', async (newOrder) => {
    console.log("newOrder: ", newOrder)
    console.log("test test test")
    const response = await fetch(`${baseURL}/dev/orders/placeOrder`, {
        method: 'post',
        body: JSON.stringify(newOrder)
    })

    console.log("placeOrder response: ", response);

    const deleteCustomerResponse = await response.json();
    return deleteCustomerResponse.data;
})

export const findOneCustomer = createAsyncThunk('customers/', async (customerId) => {
    const response = await fetch(`${baseURL}/dev/customers/findOneCustomer?customerId=${customerId}`, {
        method: 'get'
    })

    console.log(">>>>>>> response: ", response)

    const findOneCustomerResponse = await response.json();
    return findOneCustomerResponse.data;
})

const customersSlice = createSlice({
    name: 'customers',
    initialState,
    reducers: {
        getCustomers: state => {
            state.loading = true
        },
        getCustomersSuccess: (state, {payload}) => {
            state.customers = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        getCustomersFailure: state => {
            state.loading = false;
            state.hasErrors = true;
        },
    },
    extraReducers: {
        [fetchCustomersNew.pending]: (state, action) => {
          state.status = 'loading'
        },
        [fetchCustomersNew.fulfilled]: (state, action) => {
          state.status = 'succeeded'
          // Add any fetched posts to the array
          state.customers = action.payload
        },
        [fetchCustomersNew.rejected]: (state, action) => {
          state.status = 'failed'
          state.error = action.error.message
        },
        [addNewCustomer.fulfilled]: (state, action) => {
            // state.customers.push(action.payload)
            console.log('>>>>>>. addNewCustomer request fulfilled')
            console.log("action.payload: ", action.payload)
        },
        [deleteCustomer.fulfilled]: (state, action) => {
            // state.customers.push(action.payload)
            console.log('>>>>>>. addNewCustomer request fulfilled')
            console.log("action.payload: ", action.payload)
        },
        [placeOrder.fulfilled]: (state, action) => {
            console.log('>>>>>>. addNewCustomer request fulfilled')
            console.log("addNewCustomer request fulfilled action.payload: ", action.payload)
        },
        [findOneCustomer.pending]: (state, action) => {
            console.log('>>>>>>. findOneCustomer request pending');
            console.log(">>>>>>. findOneCustomer request pending action.payload: ", action.payload);
            state.selectedCustomer.loading = true;
        },
        [findOneCustomer.fulfilled]: (state, action) => {
            console.log('>>>>>>. findOneCustomer request fulfilled');
            console.log("findOneCustomer request fulfilled action.payload: ", action.payload);
            state.selectedCustomer.data = action.payload[0];
            state.selectedCustomer.hasErrors = false;
            state.selectedCustomer.loading = false;
        },
        [findOneCustomer.reject]: (state, action) => {
            console.log('>>>>>>. findOneCustomer request reject');
            console.log("findOneCustomer request reject action.payload: ", action.payload);
            state.selectedCustomer.hasErrors = true;
        },
      }
});

export const { getCustomers, getCustomersSuccess, getCustomersFailure } = customersSlice.actions;

export const customersSelector = state => state.customers;

export default customersSlice.reducer;
