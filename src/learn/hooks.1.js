import React, { useState, useContext, useEffect, useCallback, useMemo } from 'react';

const ASimpleHook = () => {
    const [count, setCount] = useState(0);
    const [obj, changeText] = useState({ 
        text1: ""
    });

    const handleSubmit = (evt) => {
        evt.preventDefault();
        alert(`Submitting Name ${obj.text1}`)
    }

    useEffect (() => {
        console.log("obj: ", obj)
        document.title = `You clicked ${count} times`
      })

    const onChangeHandler = (e) => {
        changeText({...obj, [e.target.name]: e.target.value})
    }
    return (
        <form onSubmit={handleSubmit}>
            <p>You clicked {count} times</p>
            <button onClick={() => { 
                setCount(count + 1)
                changeText()
                }
            }>
                Click me
            </button>
            <input 
                name="text1"
                value={obj.text1} 
                onChange={onChangeHandler}
            />
            <div>Some new changes {obj.text1}</div>

            <input 
                name="text2"
                value={obj.text2} 
                onChange={onChangeHandler}
            />
            <div>Some new changes {obj.text2}</div>
            <input type="submit" value="Submit" />

        </form>
    )
}


export default ASimpleHook