import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Descriptions, Badge }  from 'antd';
import {
    useParams
} from "react-router-dom";

import { findOneProduct, productsSelector } from './productsSlice';

const ShowProduct = () => {
    let { productId } = useParams();
    let dispatch = useDispatch();

    const { selectedProduct } = useSelector(productsSelector);

    useEffect(async () => {
        try {
            let resultProduct = await dispatch(findOneProduct(productId));
        } catch (err) {
            console.log("findOneProduct Error: ", err)
        }
    }, [dispatch])

    console.log("selectedProduct: ", selectedProduct);
    const {handle, title, description, sku, quantity, image_url } = selectedProduct && selectedProduct.data;
    return <div style={{background: 'white'}}>
        { selectedProduct && selectedProduct.loading &&
            <div>loading</div>
        }
        {   selectedProduct && !selectedProduct.loading &&
            <Descriptions 
            title="Product Info"
            style={{ marginTop: 20}}
            bordered
            >
                <Descriptions.Item label="Handle" span={3}>{handle}</Descriptions.Item>
                <Descriptions.Item label="Title" span={3}>{title}</Descriptions.Item>
                <Descriptions.Item label="Description" span={3}>{description}</Descriptions.Item>
                <Descriptions.Item label="SKU" span={3}>{sku}</Descriptions.Item>
                <Descriptions.Item label="Status" span={3}>
                    <Badge status="success" text="Available" />
                </Descriptions.Item>
                <Descriptions.Item label="Quantity Available" span={3}>
                    {quantity}
                </Descriptions.Item>
                <Descriptions.Item label="Product Image" span={3}>
                    <img  width="200px" height="300px" src={image_url} alt="Product image" />
                </Descriptions.Item>
            </Descriptions>
        }
    </div>
}

export default ShowProduct;