import { Component, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { 
    Row,
    Col,
    Table,
    Button
  } from 'antd';

import { UserAddOutlined } from '@ant-design/icons';

import { productsSelector, fetchProducts } from './productsSlice';

const Products = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const { products, loading, hasErrors } = useSelector(productsSelector);
    
    const handleClick = () => {
        history.push("/products/add");
    }

    const onShowClicked = async (product) => {
      console.log(">>>product: ", product)
      history.push(`/products/show/${product.product_id}`)
    }

    const columns = [
        {
          title: 'Title',
          width: 100,
          dataIndex: 'title',
          key: 'title',
          fixed: 'left',
        },
        {
          title: 'handle',
          width: 100,
          dataIndex: 'handle',
          key: 'handle',
          fixed: 'left',
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
          width: 150,
        },
        {
          title: 'SKU',
          dataIndex: 'sku',
          key: 'sku',
          width: 150,
        },
        {
          title: 'Action',
          key: 'operation',
          fixed: 'right',
          width: 100,
          render: (text, record) => <a onClick= { () => { 
            // console.log("record: ", record)
            onShowClicked(record)
          }}>Show</a>,
        },
      ];
    
    // const data = [];
    // for (let i = 0; i < 100; i++) {
    // data.push({
    //     key: i,
    //     title: `Edrward ${i}`,
    //     handle: 32,
    //     description: `London Park no. ${i}`,
    //     sku: `randomID`,
    // });
    // }
    
    console.log(">>>>>>>>: ", products)
    const data = [];
    
    products.map((item, i) => {
        data.push({
            key: i,
            product_id: item._id,
            title: item.title,
            handle: item.handle,
            description: item.description,
            sku: item.sku
        })
    })

    useEffect(() => {
        dispatch(fetchProducts())
      }, [dispatch])

    return (
        <div>
            <h3>Products</h3>
            <Row>
                <Col span={8}></Col>
                <Col span={8}></Col>
                <Col span={8}>
                    <Button 
                        type="primary" 
                        icon={<UserAddOutlined />} 
                        size={'large'}
                        onClick={handleClick}
                    >
                Add Product
                    </Button>
                </Col>
            </Row>
            <Table columns={columns} dataSource={data} scroll={{ x: 1500, y: 300 }} />
        </div>
    )
}

export default Products;