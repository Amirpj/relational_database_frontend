import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

// const baseURL = 'http://localhost:3000';
const baseURL = 'https://11fo3t57q1.execute-api.us-east-1.amazonaws.com';

export const initialState = {
    loading: false,
    hasErrors: false,
    products: [],
    selectedProduct: {
        loading: false,
        hasErrors: false,
        data: {}
    }
}

export const fetchProducts = () => {
    return async dispatch => {
        dispatch(getProducts())

        try {
            const response = await fetch(`${baseURL}/dev/products/all`)
            console.log("products response: ", response)
            const productsResponse = await response.json();

            console.log(">>>>>>>", productsResponse)

            dispatch(getProductsSuccess(productsResponse.data))
        } catch (err) {
            dispatch(getProductsFailure())
        }
    }
}

export const findOneProduct = createAsyncThunk('products/findOne', async (productId) => {
    const response = await fetch(`${baseURL}/dev/products/findOneProduct?productId=${productId}`, {
        method: 'get'
    });

    console.log(">>>>>>> findOneProduct response: ", response);

    const findOneProductResponse = await response.json();
    return findOneProductResponse.data;
});

const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        getProducts: state => {
            state.loading = true
        },
        getProductsSuccess: (state, {payload}) => {
            state.products = payload;
            state.loading = false;
            state.hasErrors = false;
        },
        getProductsFailure: state => {
            state.loading = false;
            state.hasErrors = true;
        }
    },
    extraReducers: {
        [findOneProduct.pending]: (state) => {
            console.log('>>>>>>. findOneProduct request pending');
            state.selectedProduct.loading = true;
        },
        [findOneProduct.fulfilled]: (state, action) => {
            console.log('>>>>>>. findOneProduct request fulfilled');
            console.log("findOneProduct request fulfilled action.payload: ", action.payload);
            state.selectedProduct.data = action.payload[0];
            state.selectedProduct.hasErrors = false;
            state.selectedProduct.loading = false;
        },
        [findOneProduct.reject]: (state) => {
            console.log('>>>>>>. findOneProduct request reject');
            state.selectedProduct.hasErrors = true;
        },
    }
});

export const { getProducts, getProductsSuccess, getProductsFailure } = productsSlice.actions;

export const productsSelector = state => state.products;

export default productsSlice.reducer;
