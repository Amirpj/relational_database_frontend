import { combineReducers } from 'redux';

import recipesReducer from './recipes';
import customersReducer from '../customers/customersSlice';
import ordersReducer from '../orders/ordersSlice';
import productsReducer from '../products/productsSlice';
import collectionsReducer from '../collections/collectionsSlice';


const rootReducer = combineReducers({
    recipes: recipesReducer,
    customers: customersReducer,
    orders: ordersReducer,
    products: productsReducer,
    collections: collectionsReducer,
});


export default rootReducer;